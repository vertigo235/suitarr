ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:mono
LABEL app="jackett"

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="jackett"
EXPOSE 9117
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:9117 || exit 1

# install app
# https://github.com/Jackett/Jackett/releases
RUN url="https://github.com/Jackett/Jackett/releases/download/v0.10.533/Jackett.Binaries.Mono.tar.gz" && \
    curl -fsSL "${url}" | tar xzf - -C "${APP_DIR}" --strip-components=1

COPY root/ /
