ARG REGISTRY_IMAGE
FROM ${REGISTRY_IMAGE}:base
LABEL app="ombi"

ARG DEBIAN_FRONTEND="noninteractive"

ENV APP="ombi"
EXPOSE 5000
HEALTHCHECK --interval=60s CMD curl -fsSL http://localhost:5000 || exit 1

# install packages
RUN apt update && \
    apt install -y --no-install-recommends --no-install-suggests \
        libssl1.0.0 libicu60 libunwind8 && \
# clean up
    apt autoremove -y && \
    apt clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# install app
# https://github.com/tidusjar/Ombi/releases
RUN url="https://github.com/tidusjar/Ombi/releases/download/v3.0.4036/linux.tar.gz" && \
    curl -fsSL "${url}" | tar xzf - -C "${APP_DIR}"

COPY root/ /
