#!/usr/bin/with-contenv bash
# shellcheck shell=bash

umask "${UMASK}"

if [[ "${VERSION}" != "image" ]]; then

    mkdir "${APP_DIR}-backup" && mv "${APP_DIR}"/* "${APP_DIR}-backup"
    tempfile="$(mktemp)"

    case "${APP}" in
        radarr)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/radarr/radarr/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*Radarr.*.linux.tar.gz")
                    ;;
                unstable)
                    jobid=$(curl -fsSL "https://ci.appveyor.com/api/projects/galli-leo/radarr-usby1/branch/develop" | jq -r '.build.jobs[0].jobId')
                    filename=$(curl -fsSL "https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts" | jq -r '.[].fileName' | grep -o ".*Radarr.*.linux.tar.gz")
                    url="https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts/${filename}"
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
        sonarr)
            case "${VERSION}" in
                stable)
                    url="https://download.sonarr.tv/v2/master/mono/NzbDrone.master.tar.gz"
                    ;;
                unstable)
                    url="https://download.sonarr.tv/v2/develop/mono/NzbDrone.develop.tar.gz"
                    ;;
                v3)
                    url="http://services.sonarr.tv/v1/download/phantom/latest?version=3&os=linux"
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
        lidarr)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/lidarr/lidarr/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*Lidarr.*.linux.tar.gz")
                    ;;
                unstable)
                    jobid=$(curl -fsSL "https://ci.appveyor.com/api/projects/lidarr/lidarr/branch/develop" | jq -r '.build.jobs[0].jobId')
                    filename=$(curl -fsSL "https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts" | jq -r '.[].fileName' | grep -o ".*Lidarr.*.linux.tar.gz")
                    url="https://ci.appveyor.com/api/buildjobs/${jobid}/artifacts/${filename}"
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
        jackett)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/jackett/jackett/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*Jackett.Binaries.Mono.tar.gz")
                    ;;
                unstable)
                    url=$(curl -fsSL "https://api.github.com/repos/jackett/jackett/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*Jackett.Binaries.Mono.tar.gz")
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
        nzbhydra2)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/theotherp/nzbhydra2/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*nzbhydra2-.*-linux.zip")
                    ;;
                unstable)
                    url=$(curl -fsSL "https://api.github.com/repos/theotherp/nzbhydra2/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*nzbhydra2-.*-linux.zip")
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && unzip -q "${tempfile}" -d "${APP_DIR}" && chmod +x "${APP_DIR}/nzbhydra2" && INSTALL_OK="yes"
            ;;
        nzbget)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/nzbget/nzbget/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*nzbget-.*-bin-linux.run")
                    ;;
                unstable)
                    url=$(curl -fsSL "https://api.github.com/repos/nzbget/nzbget/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*nzbget-.*-bin-linux.run")
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && sh "${tempfile}" --destdir "${APP_DIR}" && INSTALL_OK="yes"
            ;;
        bazarr)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/morpheus65535/bazarr/releases/latest" | jq -r .tarball_url)
                    ;;
                unstable)
                    url="https://github.com/morpheus65535/bazarr/archive/development.tar.gz"
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
        sabnzbd)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/sabnzbd/sabnzbd/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*SABnzbd-.*-src.tar.gz")
                    ;;
                unstable)
                    url=$(curl -fsSL "https://api.github.com/repos/sabnzbd/sabnzbd/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*SABnzbd-.*-src.tar.gz")
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
        ombi)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/tidusjar/ombi/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*linux.tar.gz")
                    ;;
                unstable)
                    url=$(curl -fsSL "https://api.github.com/repos/tidusjar/ombi/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*linux.tar.gz")
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" && INSTALL_OK="yes"
            ;;
        tautulli)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/tautulli/tautulli/releases/latest" | jq -r .tarball_url)
                    ;;
                unstable)
                    url=$(curl -fsSL "https://api.github.com/repos/tautulli/tautulli/releases" | jq -r .[0].tarball_url)
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
        nanode)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/nanocurrency/raiblocks/releases/latest" | jq -r .assets[].browser_download_url | grep -o ".*Linux.tar.bz2")
                    ;;
                unstable)
                    url=$(curl -fsSL "https://api.github.com/repos/nanocurrency/raiblocks/releases" | jq -r .[0].assets[].browser_download_url | grep -o ".*Linux.tar.bz2")
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xjf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
        nanomon)
            case "${VERSION}" in
                stable)
                    url=$(curl -fsSL "https://api.github.com/repos/nanotools/nanonodemonitor/releases/latest" | jq -r .tarball_url)
                    ;;
                unstable)
                    url="https://github.com/nanotools/nanonodemonitor/archive/develop.tar.gz"
                    ;;
                *)
                    url="${VERSION}"
                    ;;
            esac
            curl -fsSL -o "${tempfile}" "${url}" && tar xzf "${tempfile}" -C "${APP_DIR}" --strip-components=1 && INSTALL_OK="yes"
            ;;
    esac

    if [[ "${INSTALL_OK}" == "yes" ]]; then
        echo -e '\e[32m'"Installed version \"${VERSION}\"."'\e[0m'
    else
        if [[ "${FALLBACK}" == "yes" ]]; then
            mv "${APP_DIR}-backup"/* "${APP_DIR}"
            echo -e '\e[33m'"Couldn't install version \"${VERSION}\", falling back to version \"image\"."'\e[0m'
        else
            echo -e '\e[31m'"Couldn't install version \"${VERSION}\"!"'\e[0m'
        fi
    fi

    rm -rf "${APP_DIR}-backup"
    rm -f "${tempfile}"

fi

chown -R hotio:hotio "${APP_DIR}"
